# Vou Comprar

### O que é isso?

O **Vou Comprar** é um aplicativo cujo objetivo é ajudar as pessoas a manterem uma lista de desejos de compra organizada de acordo com suas capacidades financeiras. Espera-se, com isso, que os usuários consigam realizar todos os seus desejos de compra sem entrarem em prejuízo financeiro ou acabarem priorizando itens já desejados por desejos mais recentes e que nem sempre são mais importantes.

### O que tem neste repositório?

Este repositório concentra o código de *backend* que gerencia os dados exibidos no aplicativo móvel.

## Que recursos tem este aplicativo?

**Lista de desejos**

> A lista de desejos é o recurso-chave do aplicativo. Nela ficam todos os itens desejados e seus valores. A lista pode ser organizada pelo usuário.
